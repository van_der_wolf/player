$('audio[controls]');

(function ($) {
  function shuffle(array) {
    var m = array.length, t, i, map = {}
      , current = $.data($('.data'), 'current');

    // While there remain elements to shuffle…
    while (m) {

      // Pick a remaining element…
      i = Math.floor(Math.random() * m--);
      map[m] = i;
      if (current == m) {
        $.data($('.data'), 'current', i);
      }
      // And swap it with the current element.
      t = array[m];
      array[m] = array[i];
      array[i] = t;
    }
    for (var i = 0; i< map.length; i++) {
      $('div[data-index="' + i +'"]').attr('data-index', map[i]);
    }
    return array;
  }

  function playIndex(index) {
    var files = $.data($('.data')[0], 'files');
    var current = $.data($('.data')[0], 'current');
    $.data($('.data')[0], 'current', index)
    $('.player').attr('src', files[index]);
    $('.info').html(files[index]);
    $('.player').trigger('play');
  }

  function play(direction) {
    var files = $.data($('.data')[0], 'files');
    var current = $.data($('.data')[0], 'current');
    if (direction == 'next' && current <= files.length - 1) {
      $.data($('.data')[0], 'current', ++current)
    }

    if (direction == 'prev' && current >= 0) {
      $.data($('.data')[0], 'current', --current)
    }

    $('.player').attr('src', files[current]);
    $('.info').html(files[current]);
    $('.player').trigger('play');
  }

  $(document).ready(function () {
    var files = $('.file-source .file-path');
    var names = [];
    for (var i = 0; i < files.length; i++) {
      $(files[i]).attr('data-index', i);
      names.push($(files[i]).html());
    }
    $.data($('.data')[0], 'files', names);
    $.data($('.data')[0], 'current', 0);
    var player = $('.player');
    player.attr('src', $(files[0]).html());
    $('.play').click(function () {
      $('.player').trigger('play');
    });
    $('.pause').click(function () {
      $('.player').trigger('pause');
    });
    $('.stop').click(function () {
      $('.player').trigger('stop');
    });
    $('.prev').click(function () {
      play('prev');
    });
    $('.next').click(function () {
      play('next');
    });
    $('.file-source').click(function() {
      playIndex($(this).children('.file-title').data('index'));
    });
    $('.shuffle').click(function() {
      var files = shuffle($.data($('.data')[0], 'files'))
        , current = $.data($('.data'), 'current')
        , current_name = files[current]
        , new_list;

      new_list = $.data($('.data')[0], 'files', files);

    });
    player.on('ended', function () {
      play('next');
    });
    player.on('loadeddata', function() {
      var m = Math.floor(this.duration / 60),
          s = Math.floor(this.duration % 60),
          duration = ((m<10?'0':'')+m+':'+(s<10?'0':'')+s);
      $('.duration').html(duration);
      if (1){}
    })
  });
})(jQuery);