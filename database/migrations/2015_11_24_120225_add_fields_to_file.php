<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToFile extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::table('files', function ($table) {
      $table->string('title')->nullable();
      $table->string('artist', 256)->nullable();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::table('files', function ($table) {
      $table->dropColumn('title');
      $table->dropColumn('artist');
    });
  }
}
