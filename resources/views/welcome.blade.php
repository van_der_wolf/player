@extends('master')
@section('content')
    <div class="row">
        <div class="col-lg-12">

            <div class="player">
                <div class="player-content">
                    <div class="play"></div>
                    <div class="duration"></div>

                </div>
                <audio controls src="" class="player"></audio>
                <button class="play btn btn-default">Play</button>
                <button class="pause btn btn-default">Pause</button>
                <button class="stop btn btn-default">Stop</button>
                <button class="next btn btn-default">Next</button>
                <button class="prev btn btn-default">Prev</button>
                <button class="shuffle btn btn-default">shuffle</button>

                <data value="" class="data"></data>
            </div>
        </div>
        <br>
        <div class="info"></div>
        <div class="file-list col-lg-12">
         @foreach($files as $file)
                    <div class="file-source">
											<div class="file-path">{{$file->path}}</div>
                    	<span class="file-artist">{{$file->artist}}</span> - <span>{{$file->title}}</span>
                    </div>
                @endforeach
        </div>
    </div>

@endsection
