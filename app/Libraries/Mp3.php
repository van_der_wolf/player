<?php

namespace App\Libraries;
use \GetId3\GetId3Core as GetId3;
class Mp3 {

  public static function id3_get_tag($path) {
    $getId3 = new GetId3();
    $audio = $getId3
      ->setOptionMD5Data(true)
      ->setOptionMD5DataSource(true)
      ->setEncoding('UTF-8')
      ->analyze($path)
    ;
    return $audio;
  }

}