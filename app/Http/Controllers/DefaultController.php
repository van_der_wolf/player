<?php

namespace App\Http\Controllers;

use App\File;
use DB;

class DefaultController extends Controller {
  protected $redirectTo = '/';

  public function index() {
    return view('files/refresh');
  }

  public function refreshFiles() {
    function save_files($path) {
      try {
        $file_list = scandir(public_path() . $path);
        foreach ($file_list as $file) {
          if ($file != '.' && $file != '..' && !is_dir(public_path() . $path . '/' . $file) && mime_content_type(public_path() . $path . '/' . $file) == 'audio/mpeg') {
            $a = $path . '/' . $file;
            if (DB::table('files')->where('path', '=', $a)->count() == 0) {
              $metaData = \App\Libraries\Mp3::id3_get_tag(public_path() . $path . '/' . $file);
              $file = new File;
              $file->path = $a;
              $keys = array('title', 'artist');
              foreach ($keys as $key) {
                if (isset($metaData['tags']['id3v2'][$key][0])) {
                  $file->{$key} = $metaData['tags']['id3v2'][$key][0];
                } elseif(isset($metaData['tags']['id3v1'][$key][0])) {
                  $file->{$key} = $metaData['tags']['id3v1'][$key][0];
                }
              }
              $file->save();
            }
          }
          if ($file != '.' && $file != '..' && is_dir(public_path() . $path . '/' . $file)) {
            save_files($path . '/' . $file);
          }
        }
      } catch (Exception $e) {
        return redirect('/refresh-files');
      }
      return redirect('/refresh-files');
    }

    save_files('/music');
    return redirect('/refresh-files');
  }
}