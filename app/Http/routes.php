<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use App\File;

Route::get('/', function () {
  $files = File::all();
  return view('welcome', array('files' => $files));
});


Route::get('/refresh-files', ['middleware' => 'auth', 'uses' => 'DefaultController@index']);
Route::post('/refresh-files', 'DefaultController@refreshFiles'/*['middleware' => 'auth', 'uses' => 'Controller@refreshFiles']*/);

Route::resource('files', 'FilesController');
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');